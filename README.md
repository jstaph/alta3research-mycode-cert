# alta3research-mycode-cert

Provide tools to pull n number of MTG cards from the MTG api, and rehost them locally on port 9876.

## requirements

python 3.7+ (tested with python 3.10.2)

## usage

Provides a script, alta3research-pyna01.py, which has three arguments.

['-p' | '--pingcheck'] [hostname] will exit 0 if the provided host was successful, 1 if not, allowing you to check for connectivity before attempting to loaddata.
['-l' | '--loaddata' ] [n] will pull MTG card information for the first n cards released, and write that data to a file 'data.json' in the current working dir.
['-s' | '--serve' ] will host a flask server on localhost:9876 which hosts each downloaded card at localhost:9876/card/{id}

Any existing 'data.json' will be overwritten when loading data.

Please note, once '--serve' has been invoked, a CTRL-C must be used to stop the server

An example workflow might be:

```bash
python3 alta3research-mycode-cert -p 192.168.0.1 # check my local gateway, just to be certain we're online
python3 alta3research-mycode-cert -l 10 # pull the first 10 magic cards to the local 'data.json' file
python3 alta3research-mycode-cert -s # start local flask server to host the card data
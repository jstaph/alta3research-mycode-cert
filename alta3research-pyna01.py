#!/usr/bin/env python3
"""
provide a simple cli interface to check your gateway, then download N number of MTG cards
saving the output to the current directory as 'data.json'
"""
import sys


from rich import print

import capstone.loaddata

from capstone.cli.pinger import pinger
from capstone.cli import get_args
from capstone.serve import server


def main():
    args = get_args()

    if args.pingcheck:

        if pinger(args.pingcheck):
            print(
                f"test ping to [bold]{args.pingcheck}[/bold] was [green]successful[/green]"
            )
        else:
            print(
                f"test ping to [bold]{args.pingcheck}[/bold] was [red]unsuccessful[/red]"
            )
            sys.exit(1)

    if args.loaddata:
        capstone.loaddata.go(args.loaddata)

    if args.serve:
        capstone.serve.server()


if __name__ == "__main__":
    main()

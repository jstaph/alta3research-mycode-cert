""" host downloaded MTG card data """
import os
import pathlib
import json

from flask import Flask


def server():
    datafile = pathlib.Path(os.getcwd()).resolve() / "data.json"

    if datafile.exists():
        with open(datafile, "r") as data:
            # start with a null card at index '0', so our indexes match the api's
            card_data = [None]
            card_data.extend(json.load(data))
    else:
        raise Exception("'data.json' not found! Please use '-l' to load data.")

    app = Flask(__name__)

    @app.route("/health")
    def health():
        msg = f"There are {len(card_data) - 1} cards loaded"
        return json.dumps(msg)

    @app.route("/card/<card_id>")
    def card(card_id):
        try:
            card_number = int(card_id - 1)
        except ValueError:
            return json.dumps(
                f"Card {card_id} not a valid identifier.  Should be an integer"
            )

        if (card_number > len(card_data) - 1) or card_number < 0:
            return json.dumps(f"{card_id} not found")

        return json.dumps(card_data[card_number])

    app.run(host="localhost", port=9876)

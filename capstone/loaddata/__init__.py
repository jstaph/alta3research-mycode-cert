""" provide async methods to pull magic card data from the MTG API """
import os
import json
import pathlib
import asyncio
import aiohttp

MAGIC_API_BASE = "https://api.magicthegathering.io/v1/cards/"


async def load_data(n: int) -> list:
    """
    pulls the first n magic cards from api.magicthegathering.com
    """
    card_data = []
    async with aiohttp.ClientSession() as client:
        for number in range(1, n + 1):
            card_data.append(await get_card(number, client))

    return card_data


async def get_card(card_id: int, client: aiohttp.ClientSession) -> str:
    """
    return a MTG card's data from the API
    """
    async with client.get(f"{MAGIC_API_BASE}{card_id}") as resp:
        return await resp.json()


def go(n: int) -> bool:
    """
    quick method to provide a one-call fetch and write for MTG card data
    """
    card_data = asyncio.run(load_data(n))
    datafile = pathlib.Path(os.getcwd()).resolve() / "data.json"

    with open(datafile, "w", encoding="UTF-8") as data:
        data.write(json.dumps(card_data, indent=4, sort_keys=True))

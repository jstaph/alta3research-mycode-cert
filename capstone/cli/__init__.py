""" Construct and return some cli arguments for the caller """
import argparse


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    arggroup = parser.add_mutually_exclusive_group()
    arggroup.add_argument("-p", "--pingcheck", type=str, default=False)
    arggroup.add_argument("-l", "--loaddata", type=int, default=False)
    arggroup.add_argument(
        "-s", "--serve", type=bool, action=argparse.BooleanOptionalAction, default=False
    )
    return parser.parse_args()

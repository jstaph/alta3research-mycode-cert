#!/usr/bin/env python3
""" provide a way to quick-check a host before loading data """
import subprocess


def pinger(host: str) -> bool:
    """
    provides a simple way for a user of alta3rearch-pyna01.py to check
    if a gateway is available before loading data
    """

    try:
        pung = subprocess.Popen(
            ["ping", "-c", "1", host], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        pung.wait()

    except Exception:
        return False

    return bool(pung.returncode == 0)

#!/usr/bin/env python3
""" Capstone tests """
import argparse

from capstone.cli import get_args


def test_get_args():
    assert isinstance(get_args(), argparse.Namespace)
